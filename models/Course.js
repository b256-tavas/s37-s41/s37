// Setup Dependencies
const mongoose = require("mongoose");
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course Name is required"]
	},
	description: {
		type: String,
		required : [true, "Course Description is required"]
	},
	price: {
		type: String,
		required : [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		// The "new Date()" is an expression that instantiates a new date that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	// The "enrollees" property/field will be an array of objects containing the UserId and the date and time that the user enrolled
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}]
});
// model -> Course
// collections -> courses
// "module.exports" is a way for Node JS to treat this value as a "package" that can be used by other files
module.exports = mongoose.model("Course", courseSchema)