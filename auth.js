const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";


// [SECTION] JSON Web Token
/*
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
		- Information is kept secure through the use of the secret code
		- Only the system that knows the secret code that can decode the encrypted information

		- Imagine JWT as a gift wrapping service that secures the gift with a lock
		- Only the person who knows the secret code can open the lock
		- And if the wrapper has been tampered with, JWT also recognizes this a
		- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
		- This ensures that the data is secure from the sender to the receiver
*/

// Token creation
/*
- Analogy
	Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {

	// Payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
}

// Token Verification

/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/

module.exports.verify = (req, res, next) => {

// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token

	let token = req.headers.authorization

	if(typeof token !== "undefined") {

		console.log(token);
		// "slice" method -> extract a particular section of a string without modifying the original string.
		// 01234567
		// Bearer nv457nhgvh43857tf347r5783784
		// this removes the word Bearer including the space after to get the token value ONLY. 
		// token = nv457nhgvh43857tf347r5783784
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if(err) {

				return res.send({auth: "failed"})
			// If JWT is valid
			} else {
				// proceed to the next action
				next()

			}
		})
	} else {

		return res.send({auth: "failed"})
	}
}

// Token Decryption
/*
	Analogy:
		Open the gift and get the content
*/

module.exports.decode = (token) => {

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {

			if(err) {

				return null

			} else {
				// decode method -> used to obtain info from jwt
				// complete: true -> option that allows to return additional info from the JWT token
				// payload -> contains info provided in the "createAccessToken" (id, email, isAdmin)
				return jwt.decode(token, {complete: true}).payload
			}
		})
	
	} else {

		return null

	}
}





















