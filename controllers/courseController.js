const Course = require("../models/Course.js");
const auth = require("../auth.js")

// Create a new course
/*
	Business Logic:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new Course to the database
*/
module.exports.addCourse = (course) => {

	let newCourse = new Course ({
		name: course.name,
		description: course.description,
		price: course.price
	});

	return newCourse.save().then((course, err) => {

		if(err) {

				return false

			} else {

				return true;
			}
	})
}

/*
	Business Logic:
	1. Retrieve all the courses from the database
*/

module.exports.getAllCourse = () => {

	return Course.find({}).then(result => {

		return result;
	})
}

// Retrieving all active courses

module.exports.getActiveCourse = () => {

	return Course.find({isActive: true}).then(result => {

		return result;
	})
}

// Retrieving a specific course

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result;
	})
}


// Updating a course

module.exports.updateCourse = (course, paramsId) => {

	let updatedCourse = {
		name: course.name,
		description: course.description,
		price: course.price
	}

	return Course.findByIdAndUpdate(paramsId.courseId, updatedCourse).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}

// Archiving a course

module.exports.archiveCourse = (reqParams) => {

	let updatedField = {
		isActive: course.isActive
	}

	// return Course.findByIdAndUpdate(paramsId.courseId, {isActive: false})
	return Course.findByIdAndUpdate(reqParams.courseId, updatedField).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}

